package Topic_1;

/*Write a program that will accept a 4 digit number(assume that the user enters only 4 digit nos.) 
 * and print the sum of all the 4 digits. For ex : If the number passed is 3629, 
 * the program should print �The sum of all the digits entered is 20� */

import java.io.*;
public class Ass_5 {
public static void main(String args[]) throws IOException 
{
	int num,rem,sum=0;
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter a 4 digit number");
	num=Integer.parseInt(br.readLine());
	while(num>0)
	{
		rem=num%10;
		sum=sum+rem;
		num=num/10;
	}
	System.out.println("The sum of the 4 digit number :"+sum);
	
}
}
