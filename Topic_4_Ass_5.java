package Topic_4;

/*Write a program to store a group of employee names into a HashSet, retrieve the elements one by one using an Iterator*/

import java.util.*;
public class Ass_5 {
public static void main(String args[])
{
	Set<String> hs=new HashSet<String>();
	hs.add("Abarna");
	hs.add("Sujitha");
	hs.add("Indhumathi");
	Iterator<String> i=hs.iterator();
	while(i.hasNext())
	{
		System.out.println("Hash Set Elements	:"+i.next()+"\n");
	}
}
}
