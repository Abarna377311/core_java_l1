package Topic_1;

/*Write a program to print month in words, based on input month in numbers.(using switch case) */

import java.io.*;
public class Ass_4 {
public static void main(String args[]) throws NumberFormatException, IOException
{
	int mon;
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the month in numbers:");
	mon=Integer.parseInt(br.readLine());
	switch(mon)
	{
	case 1:
		System.out.println("Month you entered is January");
		break;
	case 2:
		System.out.println("Month you entered is February");
		break;
	case 3:
		System.out.println("Month you entered is March");
		break;
	case 4:
		System.out.println("Month you entered is April");
		break;
	case 5:
		System.out.println("Month you entered is May");
		break;
	case 6:
		System.out.println("Month you entered is June");
		break;
	case 7:
		System.out.println("Month you entered is July");
		break;
	case 8:
		System.out.println("Month you entered is August");
		break;
	case 9:
		System.out.println("Month you entered is September");
		break;
	case 10:
		System.out.println("Month you entered is October");
		break;
	case 11:
		System.out.println("Month you entered is November");
		break;
	case 12:
		System.out.println("Month you entered is December");
		break;
		default:
			System.out.println("Invalid month");
		
	}
}
}
