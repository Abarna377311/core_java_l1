package Topic_5_test_package;

/* Create a package called test package; Define a class called foundation inside the test package;
 *   Inside the class, you need to define 4 integer variables;  Var1 as private;  Var2 as default;  
 *   Var3 as protected;  Var4 as public;  Import this class and packages in another class. 
 *   Try to access all 4 variables of the foundation class and see what variables are accessible and what are not accessible. 
 */


public class foundation_main {
public static void main(String args[])
{
foundation f=new foundation();
//System.out.println("Var 1	:"+f.var1);    -----may not run , since var1 is private and cannot be accessed into another class
System.out.println("\nVar 1 cannot be accessed since it is private access specifier and therefore cannot access into other class");
System.out.println("\nVar 2	:"+f.var2+"--------Default access specifier");
System.out.println("\nVar 3	:"+f.var3+"--------Protected access specifier");
System.out.println("\nVar 4	:"+f.var4+"--------Public access specifier");
}
}
