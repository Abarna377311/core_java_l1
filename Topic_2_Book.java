package Topic_2;

/*
 * Write a program to create a class Book with the following  
- attributes:      -isbn, title, author, price 
  - methods :       
            i. Initialize the data members through parameterized constructor 
       ii. displaydetails() to display the details of the book 
       iii. discountedprice() : pass the discount percent, calculate the discount on price and find the amount to be paid after discount 
  - task :  
     Create an object book, initialize the book and display the details along with the discounted price
 */

public class Book {
	String isbn,title,author;
	double price;
	Book(String isbn,String title,String author,double price)
	{
		this.isbn=isbn;
		this.title=title;
		this.author=author;
		this.price=price;
	}
	void displaydetails()
	{
		System.out.println("ISBN of the book:	"+isbn+"\n");
		System.out.println("Title of the book:	"+title+"\n");
		System.out.println("Author of the book:	"+author+"\n");
		System.out.println("Price of the book:	"+price+"\n");
	}
	void discountedprice(int disc_perc)
	{
		double disc_price=(price*disc_perc)/100;
		price=price-disc_price;
		System.out.println("Amount to be paid after the discount of "+disc_perc+"% : 	"+price);
	}
	public static void main(String args[])
	{
		Book b=new Book("978-3-12424-678","JAVA","McGrawHill",1000);
		b.displaydetails();
		b.discountedprice(10);
	}
}
