package Topic_1;

/*Write a Java program to convert minutes into a number of years and days*/

import java.io.*;

public class Ass_3 {
public static void main(String args[]) throws NumberFormatException, IOException
{
	int min,days,yrs;
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the minutes to find years and days :");
	min=Integer.parseInt(br.readLine());
	yrs=min/(365*24*60);
	System.out.println("Number of years for the given minutes :"+yrs);
	days=(min/(24*60))%365;
	System.out.println("Number of days for the given minutes  :"+days);
}
}
