package Topic_3;

/*
 * Write a program to check whether the given string is a palindrome or not. 
 * [Hint :You have to extract each character from the beginning and end of the String and compare it with each other.  
 * String x=”Malayalam”; char c= x.charAt(i) where i is the index] 
 */

public class Ass_4 {
public static void main(String args[])
{
	String str="Malayalam",rev="";
	for(int i=str.length()-1;i>=0;i--)
	{
		rev=rev+str.charAt(i);
	}
	if(str.equalsIgnoreCase(rev))
		System.out.println("The given string  "+str+" is a palindrome");
	else
		System.out.println("The given string  "+str+" is not a palindrome");
}
}
