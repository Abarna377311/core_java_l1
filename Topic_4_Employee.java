package Topic_4;
import java.io.*;
public class Employee {
String ename;
int eCode;
double sal;
Employee(String ename,int eCode,double sal)
{
	this.ename=ename;
	this.eCode=eCode;
	this.sal=sal;
}
public String getEname() {
	return ename;
}
public void setEname(String ename) {
	this.ename = ename;
}
public int geteCode() {
	return eCode;
}
public void seteCode(int eCode) {
	this.eCode = eCode;
}
public double getSal() {
	return sal;
}
public void setSal(double sal) {
	this.sal = sal;
}

}
