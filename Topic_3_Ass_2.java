package Topic_3;

/*Write a Program to take care of Number Format Exception if user enters values other that integer for calculating average marks of 2 students.
 * The name of the students and marks in 3 subjects are passed as arguments while executing the program. 
 */

import java.io.*;
public class Ass_2 {
public static void main(String args[])throws IOException
{
	int eng,tam,mat,tot_1,tot_2,avg;
	String name1,name2;
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	try
	{
	System.out.println("Enter the Student name	:");
	name1=br.readLine();
	System.out.println("Enter the English marks of "+name1+":");
	eng=Integer.parseInt(br.readLine());
	System.out.println("Enter the Tamil marks of "+name1+":");
	tam=Integer.parseInt(br.readLine());
	System.out.println("Enter the Maths marks of "+name1+":");
	mat=Integer.parseInt(br.readLine());
	tot_1=eng+tam+mat;
	System.out.println("Enter the Student name	:");
	name2=br.readLine();
	System.out.println("Enter the English marks of "+name2+":");
	eng=Integer.parseInt(br.readLine());
	System.out.println("Enter the Tamil marks of "+name2+":");
	tam=Integer.parseInt(br.readLine());
	System.out.println("Enter the Maths marks of "+name2+":");
	mat=Integer.parseInt(br.readLine());
	tot_2=eng+tam+mat;
	avg=(tot_1+tot_2)/2;
	System.out.println("Average marks of 2 students	:"+avg);
	}
	catch(NumberFormatException e)
	{
		System.out.println(e);
	}
}
}
