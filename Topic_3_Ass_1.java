package Topic_3;

/*
 * Write a program to accept name and age of a person from the command prompt(passed as arguments when you execute the class)
 *  and ensure that the age entered is >=18 and < 60. Display proper error messages. 
 *  The program must exit gracefully after displaying the error message in case the arguments passed are not proper. 
 *  (Hint : Create a user defined exception class for handling errors.)
 */

import java.io.*;
public class Ass_1 {
public static void main(String args[]) throws IOException
{
	String name;
	int age;
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the name	:");
	name=br.readLine();
	System.out.println("Enter the age	:");
	age=Integer.parseInt(br.readLine());
	try
	{
	if(age<=18||age>60)
	{
		throw new Ass_1_exception();
	}
	}
	catch(Ass_1_exception e)
	{
		System.out.println(e);
	}
	
}
}
