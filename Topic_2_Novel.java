package Topic_2;
/*
 *Write a program to create a class Book with the following data members: isbn, title and price. 
 *Inherit the class Book to two derived classes : Magazine and Novel with the following data members: Magazine:    type 
 *Novel :   author 
 *Populate the details using constructors. Create a magazine and Novel and display the details.  
 */
public class Novel extends Book1 {
	String isbn,title,author;
	double price;
Novel(String isbn,String title,double price,String author)
{
	super(isbn, title,price);
	this.author=author;
}
void display()
{
	super.display();
	System.out.println("Author of the Novel	:"+author+"\n");
}
public static void main(String args[])
{
	Book1 b=new Magazine("978-12-1352376","Entrepreneur",1000,"Business");
	Book1 bo=new Novel("7869-46429-325","The White Tiger",800,"Aravind Adiga");
	b.display();
	bo.display();
}
}
