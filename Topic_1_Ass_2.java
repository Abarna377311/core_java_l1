package Topic_1;

/*
 * Write a Java program to print the result of the following operations. Declare variables and initialize them with given values  
 * a. -5 + 8 * 6 
 * b. (55+9) % 9  
 * c. 20 + -3*5 / 8  
 * d. 5 + 15 / 3 * 2 - 8 % 3
 */
public class Ass_2 {
public static void main(String args[])
{
	int a=5,b=8,c=6,d,a1=55,b1=9,d1,a2=20,b2=3,d2,d3,a3=15,b3=2;
	d=-a+b*c;
	System.out.println("Output of -5 + 8 * 6 is :"+d+"\n");
	d1=(a1+b1)%b1;
	System.out.println("Output of (55+9) % 9 is :"+d1+"\n");
	d2=a2+-b2*a/b;
	System.out.println("Output of 20 + -3*5 / 8 is	:"+d2+"\n");
	d3=a+a3/b2*b3-b%b2;
	System.out.println("Output of 5 + 15 / 3 * 2 - 8 % 3 is :"+d3+"\n");
}
}
