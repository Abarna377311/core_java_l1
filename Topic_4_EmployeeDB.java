package Topic_4;

/*Create an Employee class with the related attributes and behaviours. 
 * Create one more class EmployeeDB which has the following methods. 
 * a. boolean addEmployee(Employee e) b.  boolean deleteEmployee(int eCode)
 *  c.  String showPaySlip(int eCode)
 *   d.  Employee[] listAll() Use an ArrayList which will be used to store the emplyees and use enumeration/iterator to process the employees.
 */

import java.util.*;

public class EmployeeDB {

	List<Employee> al=new ArrayList<Employee>();
	
boolean addEmployee(Employee e)
{
	System.out.println("........Adding element into Array List........");
	return al.add(e);
}
boolean deleteEmployee(int eCode)
{
	Iterator i=al.iterator();
	while(i.hasNext())
	{
		Employee em=(Employee)i.next();
		if(em.geteCode()==eCode)
		{
			System.out.println("........Deleting employee data of "+eCode+" from the Array List.....");
			i.remove();
		}
		
	}
	return true;
}
String showPaySlip(int eCode)
{
	System.out.println("......Retrieving salary of "+eCode+"....\n");
	Iterator i=al.iterator();
	while(i.hasNext())
	{
		Employee em=(Employee)i.next();
		if(em.geteCode()==eCode)
		{
		System.out.println(eCode+"'s salary:\t"+em.sal);
		}
	}
	return "Salary data retrived";
}
Employee[] listAll()
{
	System.out.println("......Listing all the elements in the Array List.....\n");
	Employee[] emp=new Employee[al.size()];
	Iterator i=al.iterator();
	while(i.hasNext())
	{
		Employee em=(Employee)i.next();
		System.out.println("Employee Name	:"+em.ename+"\tEmployee Code	:"+em.eCode+"\tEmployee Salary	:"+em.sal+"	");
	}
	return emp;
	
}
	
public static void main(String args[])
{
	EmployeeDB ed=new EmployeeDB();
	Employee e=new Employee("Abarna",123,50000);
	Employee e1=new Employee("Indhu",456,40000);
	Employee e2=new Employee("Manjula",789,30000);
	Employee e3=new Employee("Sujitha",135,20000);
	Employee e4=new Employee("Vaishnavi",246,10000);
	List<Employee> al=new ArrayList<Employee>();
	ed.addEmployee(e);
	ed.addEmployee(e1);
	ed.addEmployee(e2);
	ed.addEmployee(e3);
	ed.addEmployee(e4);
	System.out.println("\n");
	ed.listAll();
	System.out.println("\n");
	ed.deleteEmployee(246);
	System.out.println("\n");
	ed.showPaySlip(123);
	System.out.println("\n");
	ed.listAll();
}
}
