package Topic_4;



/*Write a program creates a HashMap to store name and phone number (Telephone book).
 *  When name is give, we can get back the corresponding phone number. */

import java.io.*;
import java.util.*;
import java.util.Map.Entry;
public class Ass_4 {
public static void main(String args[]) throws IOException
{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	HashMap<String,String> hm=new HashMap<String,String>();
	hm.put("Abarna","9988766567");
	hm.put("Indhu","8976531234");
	hm.put("Manjula","7890654321");
	System.out.println("Listing all the elements of HashMap:\n");
	for(Entry<String, String> m:hm.entrySet())
	{
		 System.out.println(m.getKey()+"	"+m.getValue());
	}
	System.out.println("Enter the name to find the phone number:");
	String val=(String)hm.get(br.readLine());
	System.out.println("Phone number of the given name	:"+val);
}
}
