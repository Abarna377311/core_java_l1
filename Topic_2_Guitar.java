package Topic_2;

/*
 * Create an abstract class Instrument which is having the abstract function play.  
 * Create three more sub classes from Instrument which is Piano, Flute, Guitar. 
 * Override the play method inside all three classes printing a message  
 
�Piano is playing  tan tan tan tan  �  for Piano class 
�Flute is playing  toot toot toot toot�  for Flute class 
�Guitar is playing  tin  tin  tin �  for Guitar class  
 
You must not allow the user to declare an object of Instrument class. 
 
Create an array of 10 Instruments. 
 
Assign different type of instrument to Instrument reference. 
 
Check for the polymorphic behavior of  play method. 
 
Use the instanceof operator to print that which object stored at which index of instrument array
 */

public class Guitar extends Instrument {

	@Override
	String play() {
		// TODO Auto-generated method stub
		return"	Guitar is playing tin tin tin	";
	}
	public static void main(String args[])
	{
		Instrument inst[]=new Instrument[10];
		for(int i=0;i<inst.length;i++)
		{
			if(i==1||i==4||i==7)
				inst[i]=new Piano();
			else if(i==2||i==5||i==8)
				inst[i]=new Flute();
			else
				inst[i]=new Guitar();
			inst[i].play();
			if(inst[i] instanceof Piano)
				System.out.println("Inst["+i+"]	:	Instance of Piano"+inst[i].play());
			else if(inst[i] instanceof Flute)
				System.out.println("Inst["+i+"]	:	Instance of Flute"+inst[i].play());
			else
				System.out.println("Inst["+i+"]	:	Instance of Guitar"+inst[i].play());
			
		}
	}
	
}
