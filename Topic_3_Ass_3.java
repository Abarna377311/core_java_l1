package Topic_3;

/*Write a program to accept 5 integers passed as arguments while executing the class. 
 *Find the average of these 5 nos. 
 *Use ArrayIndexOutofBounds exception to handle situation where the user might have entered less than 5 integers.
 */

import java.io.*;
public class Ass_3 {
public static void main(String args[])throws IOException
{
	int num,sum=0;
	double avg=0;
	System.out.println("Arguments length	:"+args.length);
	
		try
		{
			if(args.length<5||args.length>5)
			{
				throw new ArrayIndexOutOfBoundsException();
			}
			else
			{
				for(int i=0;i<args.length;i++)
				{
					System.out.println("args["+i+"]	:"+args[i]);
					num=Integer.parseInt(args[i]);
					sum=sum+num;
					avg=sum/args.length;
				}
				System.out.println("Sum of the given values		:"+sum);
				System.out.println("Average of the given values	:"+avg);
			}
		}
		catch(ArrayIndexOutOfBoundsException e)
		{
			System.out.println(e);
		}
	}
}	
