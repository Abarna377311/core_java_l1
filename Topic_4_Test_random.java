package Topic_4;

/*Write  a  Java  Program,  where  one  thread  prints  a  number  (  Generate  a  random  number  using  Math.random)
 *   and  another  thread  prints  the  factorial  of  that  given  number.  Both  the  outputs  should  alternate  each  other.
 *    Eg:  Number  :  2              
 *    Factorial  of  2  :  2               
 *    Number  :  5               
 *    Factorial  of  5  :  120 
 *    The  program  can  quit  after  executing  5  times.
 *  
 */
public class Test_random {
public static void main(String args[]) throws InterruptedException {
	Random r=new Random();
	Random_Thr th=new Random_Thr(r);
	Fact_Thr tr=new Fact_Thr(r);
	for(int i=0;i<5;i++)
	{
	th.run();
	tr.run();
	}
}
}
class Random_Thr extends Thread
{
	Random r;
	Random_Thr(Random r)
	{
		this.r=r;
	}
	public void run()
	{
	r.findRandom();
	System.out.println(Thread.currentThread().getName()+"\tRandom number	:"+r.getRandom());
	}
}
class Fact_Thr extends Thread
{
	Random r;
	Fact_Thr(Random r)
	{
		this.r=r;
	}
	public void run()
	{
		System.out.println(Thread.currentThread().getName()+"\tFactorial of that number	:"+r.findfac());
	}
}